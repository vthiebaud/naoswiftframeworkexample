//
//  NaoWrapper.swift
//  NAOSwiftFrameworkExample
//
//  Created by Pole Star on 25/01/2019.
//  Copyright © 2019 Pole Star. All rights reserved.
//

import Foundation
import ModuleNAOSDK

public protocol NaoWrapperDelegate {
    func onNaoLocation(location: CLLocation!)
    func onNaoGeofenceEnter(geofenceName: String!)
    func onNaoGeofenceExit(geofenceName: String!)
    func onNaoAlert(alertContent: String!)
    func onBeaconRanged(beaconPublicID: String!)
    func onProximityChanged(proximity: String!, beaconPublicID: String!)
    func onNaoError(error: String!)
    func onNaoServiceStatusUpdated()
}

public class NaoWrapper :
    NSObject,
    NAOLocationHandleDelegate,
    NAOGeofenceHandleDelegate,
    NAOBeaconProximityHandleDelegate,
    NAOBeaconReportingHandleDelegate,
    NAOAnalyticsHandleDelegate,
    NAOSyncDelegate,
    NAOSensorsDelegate
{
    public static let wakeLockPref: String! = "wake-lock"
    var locationHandle: NAOLocationHandle?
    var geofencingHandle: NAOGeofencingHandle?
    var beaconProximityHandle: NAOBeaconProximityHandle?
    var beaconReportingHandle: NAOBeaconReportingHandle?
    var analyticsHandle: NAOAnalyticsHandle?
    var naoKey: String!
    var servicesToStart: [NAOServiceHandle] = [NAOServiceHandle].init()
    var delegate: NaoWrapperDelegate?
    public private(set) var isLocationStarted: Bool! = false
    public private(set) var isGeofencingStarted: Bool! = false
    public private(set) var isBeaconProximityStarted: Bool! = false
    public private(set) var isBeaconReportingStarted: Bool! = false
    public private(set) var isAnalyticsStarted: Bool! = false
    
    override private init() {
        super.init()
    }
    
    public init(naoKey: String!, delegate: NaoWrapperDelegate!) {
        super.init()
        self.naoKey = naoKey
        self.delegate = delegate
    }
    
    public func uploadNaoLogs() {
        NAOServicesConfig.uploadNAOLogInfo()
    }
    
    public static func isWakeLockEnabled() -> Bool! {
        let valueFound: Any? = UserDefaults.standard.object(forKey: NaoWrapper.wakeLockPref)
        return (valueFound != nil)
    }
    
    public func toggleWakeLock(enabled: Bool!) {
        if (enabled) {
            NAOServicesConfig.enableOnSiteWakeUp()
            UserDefaults.standard.set("on", forKey: NaoWrapper.wakeLockPref)
        }
        else {
            NAOServicesConfig.disableOnSiteWakeUp()
            UserDefaults.standard.removeObject(forKey: NaoWrapper.wakeLockPref)
        }
    }
    
    public func startLocation() {
        if (locationHandle == nil) {
            locationHandle = NAOLocationHandle.init(key: naoKey, delegate: self, sensorsDelegate: self)
        }
        servicesToStart.append(locationHandle!)
        locationHandle!.synchronizeData(self)
    }
    
    public func startGeofencing() {
        if (geofencingHandle == nil) {
            geofencingHandle = NAOGeofencingHandle.init(key: naoKey, delegate: self, sensorsDelegate: self)
        }
        servicesToStart.append(geofencingHandle!)
        geofencingHandle!.synchronizeData(self)
    }
    
    public func startBeaconProximity() {
        if (beaconProximityHandle == nil) {
            beaconProximityHandle = NAOBeaconProximityHandle.init(key: naoKey, delegate: self, sensorsDelegate: self)
        }
        servicesToStart.append(beaconProximityHandle!)
        beaconProximityHandle!.synchronizeData(self)
    }
    
    public func startBeaconReporting() {
        if (beaconReportingHandle == nil) {
            beaconReportingHandle = NAOBeaconReportingHandle.init(key: naoKey, delegate: self, sensorsDelegate: self)
        }
        servicesToStart.append(beaconReportingHandle!)
        beaconReportingHandle!.synchronizeData(self)
    }
    
    public func startAnalytics() {
        if (analyticsHandle == nil) {
            analyticsHandle = NAOAnalyticsHandle.init(key: naoKey, delegate: self, sensorsDelegate: self)
        }
        servicesToStart.append(analyticsHandle!)
        analyticsHandle!.synchronizeData(self)
    }
    
    public func stopLocation() {
        isLocationStarted = false
        locationHandle?.stop()
    }
    
    public func stopGeofencing() {
        isGeofencingStarted = false
        geofencingHandle?.stop()
    }
    
    public func stopBeaconProximity() {
        isBeaconProximityStarted = false
        beaconProximityHandle?.stop()
    }
    
    public func stopBeaconReporting() {
        isBeaconReportingStarted = false;
        beaconReportingHandle?.stop()
    }
    
    public func stopAnalytics() {
        isAnalyticsStarted = false
        analyticsHandle?.stop()
    }
    
    // NAO SDK callbacks
    
    public func didSynchronizationSuccess() {
        print(#function)
        for naoService in self.servicesToStart {
            naoService.start()
            if (naoService is NAOLocationHandle) {
                isLocationStarted = true
            }
            else if (naoService is NAOGeofencingHandle) {
                isGeofencingStarted = true
            }
            else if (naoService is NAOBeaconProximityHandle) {
                isBeaconProximityStarted = true
            }
            else if (naoService is NAOBeaconReportingHandle) {
                isBeaconReportingStarted = true
            }
            else if (naoService is NAOAnalyticsHandle) {
                isAnalyticsStarted = true
            }
        }
        if (self.servicesToStart.count > 0) {
            delegate?.onNaoServiceStatusUpdated()
        }
        self.servicesToStart.removeAll()
    }
    
    public func didLocationChange(_ location: CLLocation!) {
        //print(#function)
        delegate?.onNaoLocation(location: location)
    }
    
    public func didRangeBeacon(_ beaconPublicID: String!, withRssi rssi: Int32) {
        //print(#function)
        delegate?.onBeaconRanged(beaconPublicID: beaconPublicID)
    }
    
    public func didProximityChange(_ proximity: DBTBEACONSTATE, forBeacon beaconPublicID: String!) {
        print(#function)
        var proximityString: String?
        switch (proximity) {
            
        case .NO_CHANGE:
            proximityString = "No change"
        case .FIRST_UNSEEN:
            proximityString = "Unseen"
        case .FIRST_UNKNOWN:
            proximityString = "Unknown"
        case .FIRST_FAR:
            proximityString = "Far"
        case .FIRST_NEAR:
            proximityString = "Near"
        }
        delegate?.onProximityChanged(proximity: proximityString!, beaconPublicID: beaconPublicID)
    }
    
    public func didEnterGeofence(_ regionId: Int32, andName regionName: String!) {
        print(#function)
        delegate?.onNaoGeofenceEnter(geofenceName: regionName)
    }
    
    public func didExitGeofence(_ regionId: Int32, andName regionName: String!) {
        print(#function)
        delegate?.onNaoGeofenceExit(geofenceName: regionName)
    }
    
    public func didFailWithErrorCode(_ errCode: DBNAOERRORCODE, andMessage message: String!) {
        print(#function)
        delegate?.onNaoError(error: "NAO failed with message : " + message)
    }
    
    public func didSynchronizationFailure(_ errorCode: DBNAOERRORCODE, msg message: String!) {
        print(#function)
        delegate?.onNaoError(error: "NAO synchronization failed with NAO message : " + message)
    }
    
    public func requiresWifiOn() {
        print(#function)
        delegate?.onNaoError(error: "NAO needs Wi-Fi to be enabled")
    }
    
    public func requiresBLEOn() {
        print(#function)
        delegate?.onNaoError(error: "NAO needs Bluetooth LE to be enabled")
    }
    
    public func requiresLocationOn() {
        print(#function)
        delegate?.onNaoError(error: "NAO needs location to be enabled")
    }
    
    public func requiresCompassCalibration() {
        print(#function)
        delegate?.onNaoError(error: "NAO needs compass to be calibrated")
    }
    
    public func didLocationStatusChanged(_ status: DBTNAOFIXSTATUS) {
        print(#function)
        switch (status) {
        case .NAO_FIX_UNAVAILABLE:
            delegate?.onNaoError(error: "NAO services are currently unavailable")
        case .NAO_OUT_OF_SERVICE:
            delegate?.onNaoError(error: "NAO services are currently unavailable")
        case .NAO_TEMPORARY_UNAVAILABLE:
            delegate?.onNaoError(error: "NAO services are temporarily unavailable")
        case .NAO_FIX_AVAILABLE:
            print("NAO location is now available")
        }
    }
    
    public func didFire(_ alert: NaoAlert!) {
        print(#function)
        delegate?.onNaoAlert(alertContent: alert.content)
    }
    
    
}
