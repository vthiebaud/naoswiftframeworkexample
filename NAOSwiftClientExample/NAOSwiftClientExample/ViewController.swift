//
//  ViewController.swift
//  NAOSwiftClientExample
//
//  Created by Pole Star on 25/01/2019.
//  Copyright © 2019 Pole Star. All rights reserved.
//

import UIKit
import NAOSwiftFrameworkExample
import CoreLocation
import UserNotifications

class ViewController: UIViewController, NaoWrapperDelegate, UNUserNotificationCenterDelegate {

    var naoWrapper: NaoWrapper?
    @IBOutlet weak var startLocationBtn: UIButton!
    @IBOutlet weak var startGeofencingBtn: UIButton!
    @IBOutlet weak var startAnalyticsBtn: UIButton!
    @IBOutlet weak var startProximityBtn: UIButton!
    @IBOutlet weak var startReportingBtn: UIButton!
    @IBOutlet weak var enableWakeLockBtn: UIButton!
    @IBOutlet weak var startAllBtn: UIButton!
    @IBOutlet weak var uploadLogsBtn: UIButton!
    @IBOutlet weak var locationStatusLabel: UILabel!
    @IBOutlet weak var geofencingStatusLabel: UILabel!
    @IBOutlet weak var analyticsStatusLabel: UILabel!
    @IBOutlet weak var proximityStatusLabel: UILabel!
    @IBOutlet weak var reportingStatusLabel: UILabel!
    @IBOutlet weak var wakeLockStatusLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var naoOutputTextView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        UNUserNotificationCenter.current().delegate = self
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { (granted, error) in
            if !granted {
                print("Notifications disabled")
            }
        }
        
        self.naoWrapper = NaoWrapper.init(naoKey: "oarWVFpQUbccjy7ZBZCaOQ", delegate: self)
        self.updateUI()
    }
    
    // Buttons callbacks
    
    @IBAction func onStartLocationClicked(_ sender: Any) {
        if (naoWrapper?.isLocationStarted ?? false) {
            naoWrapper?.stopLocation()
        }
        else {
            naoWrapper?.startLocation()
        }
        self.updateUI()
    }
    
    @IBAction func onStartGeofencingClicked(_ sender: Any) {
        if (naoWrapper?.isGeofencingStarted ?? false) {
            naoWrapper?.stopGeofencing()
        }
        else {
            naoWrapper?.startGeofencing()
        }
        self.updateUI()
    }
    
    @IBAction func onStartAnalyticsClicked(_ sender: Any) {
        if (naoWrapper?.isAnalyticsStarted ?? false) {
            naoWrapper?.stopAnalytics()
        }
        else {
            naoWrapper?.startAnalytics()
        }
        self.updateUI()
    }
    
    @IBAction func onBeaconProximityClicked(_ sender: Any) {
        if (naoWrapper?.isBeaconProximityStarted ?? false) {
            naoWrapper?.stopBeaconProximity()
        }
        else {
            naoWrapper?.startBeaconProximity()
        }
        self.updateUI()
    }
    
    @IBAction func onBeaconReportingClicked(_ sender: Any) {
        if (naoWrapper?.isBeaconReportingStarted ?? false) {
            naoWrapper?.stopBeaconReporting()
        }
        else {
            naoWrapper?.startBeaconReporting()
        }
        self.updateUI()
    }
    
    @IBAction func onEnableWakeLockClicked(_ sender: Any) {
        if (NaoWrapper.isWakeLockEnabled()) {
            naoWrapper?.toggleWakeLock(enabled: false)
        }
        else {
            naoWrapper?.toggleWakeLock(enabled: true)
        }
        self.updateUI()
    }
    
    @IBAction func onStartAllServicesClicked(_ sender: Any) {
        naoWrapper?.startLocation()
        naoWrapper?.startGeofencing()
        naoWrapper?.startBeaconReporting()
        naoWrapper?.startBeaconProximity()
        naoWrapper?.startAnalytics()
    }
    
    @IBAction func onUploadLogsClicked(_ sender: Any) {
        if (naoWrapper != nil) {
            naoWrapper?.uploadNaoLogs();
            output("NAO logs upload scheduled")
        }
    }
    
    // NaoWrapper callbacks
    
    func onNaoServiceStatusUpdated() {
        print(#function)
        self.updateUI()
    }
    
    func onNaoLocation(location: CLLocation!) {
        //print(#function)
        self.locationLabel.text = "Lat : " + location.coordinate.latitude.description +
            "\nLon : " + location.coordinate.longitude.description +
            "\nAlt : " + location.altitude.description
    }
    
    func onNaoGeofenceEnter(geofenceName: String!) {
        print(#function)
        self.output("Entered geofence : " + geofenceName)
    }
    
    func onNaoGeofenceExit(geofenceName: String!) {
        print(#function)
        self.output("Left geofence : " + geofenceName)
    }
    
    func onNaoAlert(alertContent: String!) {
        print(#function)
        self.output("Received NAO alert " + alertContent)
    }
    
    func onBeaconRanged(beaconPublicID: String!) {
        self.output("Ranged beacon : " + beaconPublicID)
    }
    
    func onProximityChanged(proximity: String!, beaconPublicID: String!) {
        print(#function)
        self.output("Proximity changed to " + proximity + " for beacon " + beaconPublicID)
    }
    
    func onNaoError(error: String!) {
        print(#function)
        self.output("Received NAO error : " + error)
    }
    
    func output(_ message: String!) {
        if (self.naoOutputTextView.text.count > 1024) {
            self.naoOutputTextView.text = ""
        }
        self.naoOutputTextView.text += message + "\n" + self.naoOutputTextView.text
    }
    
    // UI helper methods
    
    func updateServiceUI(_ isStarted: Bool!, serviceName: String!, startButton: UIButton!, statusLabel: UILabel!) {
        if (isStarted) {
            startButton.setTitle("Stop " + serviceName, for: UIControl.State.normal)
            statusLabel.text = "On"
        }
        else {
            startButton.setTitle("Start " + serviceName, for: UIControl.State.normal)
            statusLabel.text = "Off"
        }
    }
    
    func updateUI() {
        self.updateServiceUI(naoWrapper?.isLocationStarted ?? false, serviceName: "Location",
                             startButton: self.startLocationBtn, statusLabel: self.locationStatusLabel)
        self.updateServiceUI(naoWrapper?.isGeofencingStarted ?? false, serviceName: "Geofencing",
                             startButton: self.startGeofencingBtn, statusLabel: self.geofencingStatusLabel)
        self.updateServiceUI(naoWrapper?.isBeaconProximityStarted ?? false, serviceName: "Beacon proximity",
                             startButton: self.startProximityBtn, statusLabel: self.proximityStatusLabel)
        self.updateServiceUI(naoWrapper?.isBeaconReportingStarted ?? false, serviceName: "Beacon reporting",
                             startButton: self.startReportingBtn, statusLabel: self.reportingStatusLabel)
        self.updateServiceUI(naoWrapper?.isAnalyticsStarted ?? false, serviceName: "Analytics",
                             startButton: self.startAnalyticsBtn, statusLabel: self.analyticsStatusLabel)
        if (NaoWrapper.isWakeLockEnabled()) {
            self.enableWakeLockBtn.setTitle("Disable " + "Wake Lock", for: UIControl.State.normal)
            self.wakeLockStatusLabel.text = "On"
        }
        else {
            self.enableWakeLockBtn.setTitle("Enable " + "Wake Lock", for: UIControl.State.normal)
            self.wakeLockStatusLabel.text = "Off"
        }
    }
    
    // Notification center delegate methods
    public func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler([.alert, .badge, .sound])
    }
    
    public func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        self.naoWrapper?.startLocation()
        self.updateUI()
    }
}
